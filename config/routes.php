<?php

$routes = array(
    array('home','default','index'),

    // admin
    array('admin','admin','index'),

    // User
    array('ajoutUser', 'users', 'addUser'),

    // Salle
    array('ajoutSalle','salle','addSalle'),

    // Creneau
    array('ajoutCreneau','creneau','addCreneau'),
    array('singleCreneau','creneau','single',array('id')),
);
