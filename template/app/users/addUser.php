<p style="text-align: center;font-size:33px;font-weight: bold;margin: 50px 0;color:#a84743;"> <?= $message ?> </p>

<form action="" method="post" novalidate class="wrapform">
    <?= $addUser->label('nom'); ?>
    <?= $addUser->input('nom'); ?>
    <?= $addUser->error('nom'); ?>

    <?= $addUser->label('email'); ?>
    <?= $addUser->input('email','email'); ?>
    <?= $addUser->error('email'); ?>

    <?= $addUser->submit() ?>
</form>
