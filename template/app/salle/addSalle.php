<p style="text-align: center;font-size:33px;font-weight: bold;margin: 50px 0;color:#a84743;"> <?= $message ?> </p>

<form action="" method="post" novalidate class="wrapform">
    <?= $addSalle->label('title'); ?>
    <?= $addSalle->input('title'); ?>
    <?= $addSalle->error('title'); ?>

    <?= $addSalle->label('maxuser'); ?>
    <?= $addSalle->input('maxuser','number'); ?>
    <?= $addSalle->error('maxuser'); ?>

    <?= $addSalle->submit() ?>
</form>
