<p style="text-align: center;font-size:33px;font-weight: bold;margin: 50px 0;color:#a84743;"> <?= $message ?> </p>

<form action="" method="post" novalidate class="wrapform">
    <?= $addCreneau->label('salle'); ?><br>
    <?= $addCreneau->selectEntity('salle',$selectSalle,'title'); ?><br>
    <?= $addCreneau->error('salle'); ?>

    <?= $addCreneau->label('start_at','Quand'); ?>
    <?= $addCreneau->input('start_at','datetime-local'); ?>
    <?= $addCreneau->error('start_at'); ?>

    <?= $addCreneau->label('nbrehours','durée'); ?>
    <?= $addCreneau->input('nbrehours','time'); ?>
    <?= $addCreneau->error('nbrehours'); ?>

    <?= $addCreneau->submit() ?>
</form>
