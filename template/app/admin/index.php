<p style="text-align: center;font-size:33px;font-weight: bold;margin: 50px 0;color:#a84743;"> <?= $message ?> </p>

<section id="table">
    <div id="User">
        <a href="<?= $view->path('ajoutUser'); ?>">Ajouter un User</a>
        <table>
            <thead>
            <tr>
                <th>Nom</th>
                <th>Email</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($allUsers as $allUser){ ?>
                <tr>
                    <td><?= $allUser->nom?></td>
                    <td><?= $allUser->email?></td>
                    <td><a class="supprimer" onclick="return confirm('Voulez-vous effacer Utilisateur?')" href="<?= $view->path('deletUser', array('id'=>$allUser->id)) ?>">Supprimer?</a></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>


    <div id="Salle">
        <a href="<?= $view->path('ajoutSalle'); ?>">Ajouter une Salle</a>
        <table>
            <thead>
            <tr>
                <th>Titre</th>
                <th>Nombre de place</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($allSalles as $allSalle){ ?>
                <tr>
                    <td><?= $allSalle->title?></td>
                    <td><?= $allSalle->maxuser?></td>
                    <td><a class="supprimer" onclick="return confirm('Voulez-vous effacer Salle?')" href="<?= $view->path('deletSalle', array('id'=>$allSalle->id)) ?>">Supprimer?</a></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>

    <div id="Creneau">
        <a href="<?= $view->path('ajoutCreneau'); ?>">Ajouter un Creneau</a>
        <table>
            <thead>
            <tr>
                <th>Salle</th>
                <th>Date de commencement</th>
                <th>Durée</th>
                <th>Détail</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($allCreneaux as $allCreneau){
                $titleSalleById = \App\Model\SalleModel::findById($allCreneau->getIdSalle())
                ?>
                <tr>
                    <td><?= $titleSalleById->title?></td>
                    <td><?= $allCreneau->getStartAt()?></td>
                    <td><?= $allCreneau->getNbrehours()?></td>
                    <td><a href="<?= $view->path('singleCreneau', array('id'=>$allCreneau->id)) ?>">detail</a></td>
                    <td><a class="supprimer" onclick="return confirm('Voulez-vous effacer Creneau?')" href="<?= $view->path('deletCreneau', array('id'=>$allCreneau->id)) ?>">Supprimer?</a></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</section>

