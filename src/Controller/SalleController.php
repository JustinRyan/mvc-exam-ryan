<?php

namespace App\Controller;

use App\Model\SalleModel;
use App\Service\Form;
use App\Service\Validation;

class SalleController extends BaseController {

    public function addSalle() {
        $message = 'Creer une Salle';

        $error = array();
        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);

            $valid = new Validation();
            $error = $this->validate($valid,$post);

            if($valid->IsValid($error)) {
                SalleModel::insert($post);
                $this->addFlash('success', 'Salle ajouter.');
                $this->redirect('admin');
            }
        }

        $addSalle = new Form($error);

        $this->render('app.salle.addSalle',array(
            'message' => $message,
            'addSalle' => $addSalle,
        ));
    }

    private function validate($valid,$post) {
        $error['title'] = $valid->textValid($post['title'], 'titre', 2, 20);
        if ($post['maxuser'] < 1){
            $error['maxuser'] = 'il doit pouvoir y avoir au moins une personne';

        }
        if ($post['maxuser'] >= 10000) {
            $error['maxuser'] = 'je pense que ca fait beaucoup la non?';
        }

        return $error;
    }
}