<?php

namespace App\Controller;

use App\Model\CreneauModel;
use App\Model\SalleModel;
use App\Service\Form;
use App\Service\Validation;

class CreneauController extends BaseController {

    public function single($id) {
        $message = 'Info sur le creneau';

        $getor404 = $this->getCreneauByIdOr404($id);

        $allCreneau = CreneauModel::findById($id);

        $getSalle = SalleModel::findById($allCreneau->getIdSalle());

        $this->render('app.creneau.single',array(
            'message' => $message,
            'getor404' => $getor404,
            'allCreneau' => $allCreneau,
            'getSalle' => $getSalle,
        ));
    }

    public function addCreneau() {
        $message = 'Creer une Creneau';

        $error = array();
        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);

            $valid = new Validation();
            $error = $this->validate($valid,$post);

            if($valid->IsValid($error)) {
                CreneauModel::insert($post);
                $this->addFlash('success', 'Creneau ajouter.');
                $this->redirect('admin');
            }
        }

        $selectSalle = SalleModel::all();
        $addCreneau = new Form($error);

        $this->render('app.creneau.addCreneau',array(
            'message' => $message,
            'addCreneau' => $addCreneau,
            'selectSalle' => $selectSalle,
        ));
    }

    private function validate($valid,$post) {
        if (!SalleModel::findById($post['salle'])){
            $error['salle'] = 'STOP HAKING';
        }

        $error['start_at'] = $valid->textValid($post['start_at'], 'debut', 16, 16);
        if (!$error['start_at'] && $post['start_at'] < date("Y-m-d H:i:s")){
            $error['start_at'] = 'vous en pouvez pas commencer dans le passée';
        }

        $error['nbrehours'] = $valid->textValid($post['nbrehours'], 'temps', 5, 5);

        return $error;
    }

    private function getCreneauByIdOr404($id) {
        $creneau = CreneauModel::findById($id);
        if (empty($creneau)) {
            $this->Abort404();
        }
        return $creneau;
    }
}