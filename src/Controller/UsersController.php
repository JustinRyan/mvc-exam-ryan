<?php

namespace App\Controller;

use App\Model\UsersModel;
use App\Service\Form;
use App\Service\Validation;

class UsersController extends BaseController {

    public function addUser() {
        $message = 'Creer un utilisateur';

        $error = array();
        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);

            $valid = new Validation();
            $error = $this->validate($valid,$post);

            if($valid->IsValid($error)) {
                UsersModel::insert($post);
                $this->addFlash('success', 'Utilisateur ajouter.');
                $this->redirect('admin');
            }
        }

        $addUser = new Form($error);

        $this->render('app.users.addUser',array(
            'message' => $message,
            'addUser' => $addUser,
        ));
    }

    private function validate($valid,$post) {
        $error['nom'] = $valid->textValid($post['nom'], 'nom', 2, 20);
        $error['email'] = $valid->emailValid($post['email']);

        return $error;
    }
}