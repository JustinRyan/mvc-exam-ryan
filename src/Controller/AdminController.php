<?php

namespace App\Controller;

use App\Model\CreneauModel;
use App\Model\SalleModel;
use App\Model\UsersModel;

class AdminController extends BaseController {
    public function index() {
        $message = 'Acceuil';

        $allUsers = UsersModel::all();
        $allSalles = SalleModel::all();
        $allCreneaux = CreneauModel::all();

//        $this->dump($allCreneaux);
        $this->render('app.admin.index',array(
            'message' => $message,
            'allUsers' => $allUsers,
            'allSalles' => $allSalles,
            'allCreneaux' => $allCreneaux,
        ));
    }
}
