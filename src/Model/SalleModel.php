<?php

namespace App\Model;


use Core\App;
use Core\Kernel\AbstractModel;

class SalleModel extends AbstractModel {
    protected static $table = 'salle';

    protected $id;
    protected $title;
    protected $maxuser;

    public static function insert($post) {
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . " (title,maxuser) VALUES (?,?)",
            [$post['title'],$post['maxuser']]
        );
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return mixed
     */
    public function getMaxuser()
    {
        return $this->maxuser;
    }
}